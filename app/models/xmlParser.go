package models

import (
	"encoding/xml"
	"errors"
	"io/ioutil"
	"os"
)

type XmlParser struct {
	SdnList SdnList
}

func (parser *XmlParser) DocumentParse(filePath string) error {
	println("parsing file")
	file, err := os.Open(filePath)
	if err != nil {
		return errors.New(err.Error())
	}
	defer file.Close()

	byteValue, err := ioutil.ReadAll(file)
	if err != nil {
		return errors.New(err.Error())
	}

	error := xml.Unmarshal(byteValue, &parser.SdnList)
	if error != nil {
		return errors.New(err.Error())
	}

	println("parsing complete")
	return nil
}
