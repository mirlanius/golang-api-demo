package models

import (
	"errors"
	"io"
	"net/http"
	"os"
)

type DownloadDoc struct {
	Status     string
	Url        string
	PathToSave string
}

const (
	STATUS_INPROCESS string = "updating"
	STATUS_READY     string = "ok"
	STATUS_EMPTY     string = "empty"
)

func (doc *DownloadDoc) Download() error {
	println("start download")
	doc.Status = STATUS_INPROCESS
	file, err := os.Create(doc.PathToSave)
	if err != nil {
		return errors.New("path to save file not fount")
	}
	defer file.Close()

	resp, err := http.Get(doc.Url)
	if err != nil {
		return errors.New("error access to url")
	}

	_, errCopy := io.Copy(file, resp.Body)
	if errCopy != nil {
		return errors.New("error save file")
	}
	doc.Status = STATUS_READY
	println("download complete")
	return nil
}
