package models

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"mirlans.com/golangdemo/app/config"
)

func DataBase() *sql.DB {
	config := config.ConfigInstance()
	dblink := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.DB_HOST, config.DB_PORT, config.DB_USER, config.DB_PASS, config.DB_NAME)

	db, err := sql.Open("postgres", dblink)
	if err != nil {
		panic(err)
	}
	return db
}
