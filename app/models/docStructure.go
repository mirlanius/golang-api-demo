package models

import "encoding/xml"

type SdnList struct {
	XMLName           xml.Name          `xml:"sdnList"`
	PublshInformation PublshInformation `xml:"publshInformation"`
	SdnEntries        []SdnEntry        `xml:"sdnEntry"`
}

type PublshInformation struct {
	XMLName     xml.Name `xml:"publshInformation"`
	PublishDate string   `xml:"Publish_Date"`
	RecordCount int      `xml:"Record_Count"`
}

type SdnEntry struct {
	XMLName     xml.Name   `xml:"sdnEntry"`
	Uid         int        `xml:"uid"`
	LastName    string     `xml:"lastName"`
	SdnType     string     `xml:"sdnType"`
	ProgramList []Program  `xml:"ProgramList"`
	AkaList     AkaList    `xml:"akaList"`
	AddressList AdressList `xml:"addressList"`
}

type Program struct {
	XMLName xml.Name `xml:"program"`
}

type AkaList struct {
	XMLName xml.Name `xml:"akaList"`
	Aka     []Aka    `xml:"aka"`
}

type Aka struct {
	XMLName  xml.Name `xml:"aka"`
	Uid      int      `xml:"uid"`
	Type     string   `xml:"type"`
	Category string   `xml:"category"`
	LastName string   `xml:"lastName"`
}

type AdressList struct {
	XMLName xml.Name  `xml:"addressList"`
	Address []Address `xml:"address"`
}

type Address struct {
	XMLName xml.Name `xml:"address"`
	Uid     int      `xml:"uid"`
	City    string   `xml:"city"`
	Country string   `xml:"country"`
}
