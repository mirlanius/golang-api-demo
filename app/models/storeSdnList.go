package models

import (
	"fmt"
	"strings"
)

const (
	SEARCH_WEAK   = "weak"
	SEARCH_STRONG = "strong"
)

type StoreSdn struct{}
type NameElement struct {
	Uid  int
	Name string
}

const TYPE_INDIVIDUAL = "Individual"
const batchSize int = 100

func (store *StoreSdn) SaveIndividualOnly(sdnList *SdnList) error {
	database := DataBase()
	defer database.Close()

	insertValues := []string{}
	counter := 0
	savedRows := 0
	for _, entry := range sdnList.SdnEntries {
		if entry.SdnType != TYPE_INDIVIDUAL {
			continue
		}
		entry.LastName = strings.ReplaceAll(entry.LastName, "'", "`")
		value := fmt.Sprintf("(%d, '%s')", entry.Uid, entry.LastName)
		insertValues = append(insertValues, value)
		counter++
		if counter >= batchSize {
			tx, err := database.Begin()
			statement := fmt.Sprintf(`INSERT INTO entries(uuid, last_name) VALUES %s`, strings.Join(insertValues, ","))

			tx.Exec(statement)
			if err != nil {
				tx.Rollback()
			}
			tx.Commit()
			savedRows++
			counter = 0
			insertValues = nil
		}
	}

	fmt.Printf("save complete, affected %d rows", savedRows)
	return nil
}

func (store *StoreSdn) GetNames(name string, searcType string) ([]NameElement, error) {
	database := DataBase()
	defer database.Close()
	var names []NameElement

	if name == "" {
		names = append(names, NameElement{})
		return names, nil
	}

	var where string
	switch searcType {
	case SEARCH_WEAK:
		where = "WHERE last_name LIKE " + "'%" + name + "%'"
	case SEARCH_STRONG:
		where = "WHERE last_name = '" + name + "'"
	default:
		where = "WHERE last_name LIKE " + "'%" + name + "%'"
	}

	query := "SELECT * FROM entries " + where
	fmt.Println("GetNames start query, ", query)
	rows, err := database.Query(query)
	if err != nil {
		fmt.Println("get names, database query error ", err.Error())
		panic(err.Error())
	}

	for rows.Next() {
		var name NameElement
		err := rows.Scan(&name.Uid, &name.Name)
		if err != nil {
			return names, err
		}
		names = append(names, name)
	}

	return names, nil
}
