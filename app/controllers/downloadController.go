package controllers

import (
	"log"
	"net/http"

	"mirlans.com/golangdemo/app/config"
	"mirlans.com/golangdemo/app/models"
	"mirlans.com/golangdemo/app/views"
)

var download models.DownloadDoc = models.DownloadDoc{
	Url:        config.ConfigInstance().DownloadUrl,
	PathToSave: config.ConfigInstance().DocFilePath,
}

var docParser models.XmlParser = models.XmlParser{}

func DownloadDocument(resp http.ResponseWriter, req *http.Request) {
	var storeSdn models.StoreSdn = models.StoreSdn{}

	info := views.ResponceInfo{
		Result: true,
		Info:   "",
		Code:   http.StatusOK,
	}

	// download from url
	err_download := download.Download()
	if err_download != nil {
		log.Fatal(err_download.Error())
	}

	// parse loaded file
	err_parser := docParser.DocumentParse(config.ConfigInstance().DocFilePath)
	if err_parser != nil {
		log.Fatal(err_parser.Error())
	}

	//TODO: check xml datetime for skip save to db

	// save to database
	err_db := storeSdn.SaveIndividualOnly(&docParser.SdnList)
	if err_db != nil {
		panic(err_db.Error())
	}

	if err_download != nil || err_parser != nil || err_db != nil {
		info.Info = "service unavailable"
		info.Code = http.StatusBadGateway
	}

	views.JsonRender(info, resp)
}

func State(resp http.ResponseWriter, req *http.Request) {
	info := views.ResponceInfo{
		Result: true,
		Info:   download.Status,
		Code:   http.StatusOK,
	}

	views.JsonRender(info, resp)
}
