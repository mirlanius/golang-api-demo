package controllers

import (
	"net/http"

	"mirlans.com/golangdemo/app/models"
	"mirlans.com/golangdemo/app/views"
)

func GetNames(resp http.ResponseWriter, req *http.Request) {
	var storeSdn models.StoreSdn = models.StoreSdn{}
	var statusCode int = http.StatusOK

	name := req.URL.Query().Get("name")
	searchType := req.URL.Query().Get("type")

	names, err := storeSdn.GetNames(name, searchType)
	if err != nil {
		statusCode = http.StatusBadGateway
	}

	info := views.ResponceGetNames{
		Code: statusCode,
		Data: names,
	}
	views.GetNames(info, resp)
}
