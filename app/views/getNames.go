package views

import (
	"encoding/json"
	"net/http"

	"mirlans.com/golangdemo/app/models"
)

type ResponceGetNames struct {
	Code int
	Data []models.NameElement
}

func GetNames(info ResponceGetNames, w http.ResponseWriter) {
	w.WriteHeader(info.Code)
	json.NewEncoder(w).Encode(info.Data)
}

func EmptyData(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write(nil)
}
