package views

import (
	"encoding/json"
	"net/http"
)

type ResponceInfo struct {
	Result bool   `json:"result"`
	Info   string `json:"info"`
	Code   int    `json:"code"`
}

func JsonRender(info ResponceInfo, w http.ResponseWriter) {
	w.WriteHeader(info.Code)
	json.NewEncoder(w).Encode(info)
}
