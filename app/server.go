package app

import (
	"log"
	"net/http"

	"mirlans.com/golangdemo/app/controllers"
)

func Serve() {

	http.HandleFunc("/update", controllers.DownloadDocument)
	http.HandleFunc("/state", controllers.State)
	http.HandleFunc("/get_names", controllers.GetNames)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
