package config

import (
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

type Config struct {
	DownloadUrl string
	DocFilePath string
	DB_USER     string
	DB_PORT     int
	DB_PASS     string
	DB_HOST     string
	DB_NAME     string
}

var instance *Config = nil

//Singletone
func ConfigInstance() Config {
	if instance != nil {
		return *instance
	}

	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}

	conf := Config{}
	conf.DownloadUrl = getKey("DOWNLOAD_LINK", "")
	conf.DocFilePath = getKey("PATH_TO_SAVE_DOCUMENT", "./var/sdn.xml")
	conf.DB_USER = getKey("DATABASE_USER", "root")
	port_db, _ := strconv.Atoi(getKey("DATABASE_PORT", "5432"))
	conf.DB_PORT = port_db
	conf.DB_PASS = getKey("DATABASE_PASS", "")
	conf.DB_HOST = getKey("DATABASE_HOST", "127.0.0.1")
	conf.DB_NAME = getKey("DATABASE_NAME", "")

	instance = &conf
	return conf
}

// get env key with default value
func getKey(key string, defValue string) string {
	value := os.Getenv(key)
	if len(value) > 0 {
		return value
	} else {
		return defValue
	}
}
