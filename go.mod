module mirlans.com/golangdemo

go 1.18

require (
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/lib/pq v1.10.8 // indirect
)
