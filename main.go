package main

import (
	"fmt"

	app "mirlans.com/golangdemo/app"
)

func main() {
	fmt.Println("server start")
	app.Serve()
}
