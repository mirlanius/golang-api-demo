CREATE TABLE
entries(
    uuid varchar(255) PRIMARY KEY,
    last_name varchar(255)
);
CREATE INDEX entries_last_name ON entries(last_name);